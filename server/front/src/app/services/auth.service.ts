import { Injectable, ɵCodegenComponentFactoryResolver } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
const URL = "http://localhost:3000/contribution/";
const jwtHelper = new JwtHelperService();
const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  connectedUser: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  currentConnectedUser = this.connectedUser.asObservable();

  constructor(public http: HttpClient) {
    this.loadJWT();
  }

  signIn(login, password) {
    const data = {
      login: login,
      password: password
    }
    console.log(data)
    return new Promise((resolve, reject) => {
      this.http.post<any>(URL + "login", data, httpOptions).subscribe(resp => {
        console.log(resp)
        if(resp["success"] == true){
          localStorage.setItem("token", resp["data"]);
          const jwt = jwtHelper.decodeToken(resp["data"]);
          this.changeCurrentUserData(jwt["login"]);
          resolve(true)
        }else 
        resolve(false)
        
      }, error => {
        const errorMsg = error.status === 401 ? 'Invalid login or password' : 'Unable to reach server';
        reject(errorMsg);
      });
    })
  }

  signOut() {
    this.changeCurrentUserData(null);
    localStorage.removeItem("token");
  }


  changeCurrentUserData(login) {
    this.connectedUser.next(login);
  }

  getConnectedUser(): string {
    return this.connectedUser.value;
  }

  jwtIsExpired(token: string) {
    return jwtHelper.isTokenExpired(token);
  }

  loadJWT() {
    const jwt = localStorage.getItem("token");
    if (jwt) {
      if (!this.jwtIsExpired(jwt)) {
        const jwtData = jwtHelper.decodeToken(jwt);
        this.changeCurrentUserData(jwtData["login"]);
      }
    }
  }


}
