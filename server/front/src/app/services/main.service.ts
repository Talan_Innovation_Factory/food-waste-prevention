import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  url: string = "http://localhost:8000";

  constructor(public http: HttpClient) {
  }
  getSessions(){
    return this.http.get(this.url + '/getTrainingSessions');
  }
  getContributions(){
    return this.http.get(this.url + '/getContributions');
  }
  launchFL(data){
    return this.http.post(this.url + '/launchFL', {
      params: data});
  }
}
