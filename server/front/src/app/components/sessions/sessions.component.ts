import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MainService } from 'src/app/services/main.service';

@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.scss']
})
export class SessionsComponent implements OnInit {
  sessions: any;
  num_rounds;
  constructor(private mainService: MainService, private toastr: ToastrService) { }

  ngOnInit() {
    this.mainService.getSessions().subscribe(res => {
      this.sessions = res;
    })
  }
  launchSession(){
    const data = {
      num_rounds : this.num_rounds,
      ipaddress: "127.0.0.1",
      port : 4500,
      resume: true
    }
    this.toastr.success('Started Successfuly!', 'Training Session!');
    this.mainService.launchFL(data).subscribe(res => {

    }, err => {
      this.toastr.error('Training Session!', 'Error while starting session!');
    });
    
    
  }
}
