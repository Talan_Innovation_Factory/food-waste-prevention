import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  errorMsg: string = '';
  loading: boolean = false;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.createLoginForm();
  }
  login() {
    this.loading = true;
      let login = this.loginForm.get("login").value;
      let password = this.loginForm.get("password").value;
    this.authService.signIn(login, password).then(response => {
        this.loading = false;
        if(response)
        this.router.navigateByUrl('/dashboard');
    }).catch(err=> {
      this.loading = false;
      this.errorMsg = (err as string);
    });
   
  }


  createLoginForm() {
    this.loginForm = new FormGroup({
      login: new FormControl('', [
        Validators.required,
      ]),
      password: new FormControl('', [
        Validators.required,
      ]),
    });
  }

}
