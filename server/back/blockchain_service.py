from typing import Optional
from fastapi import FastAPI
from web3.auto import w3
from web3 import Web3
import json
import time
import asyncio
from threading import Thread

contributionSC = open('./config/contracts/build/Contribution.json',)
federationSC = open('./config/contracts/build/federation.json',)
contributionData = json.load(contributionSC)
federationData = json.load(federationSC)
contributionAbi = contributionData['abi']
federationAbi = federationData['abi']
addressContribution = contributionData["networks"]['5777']["address"]
addressFederation = federationData["networks"]['5777']["address"]
w3 = Web3(Web3.HTTPProvider('http://127.0.0.1:8545'))
contribution_contract_instance = w3.eth.contract(address=addressContribution, abi=contributionAbi)
federation_contract_instance = w3.eth.contract(address=addressFederation, abi=federationAbi)
event_filter = contribution_contract_instance.events.contribEvent.createFilter(fromBlock='latest')
poll_interval = 2
class BlockchainService():
    def getContributions(self):
        addresses = contribution_contract_instance.functions.get_edge_addresses().call()
        roundNumbers = contribution_contract_instance.functions.get_rNos().call()
        results = []
        for address in addresses:
            for rNo in roundNumbers:
                contribution = contribution_contract_instance.functions.get_contribution(address, rNo).call()
                print(contribution)
                contrib_dic={}
                contrib_dic[0]= contribution[0]
                contrib_dic[1]= contribution[1]
                contrib_dic[2]= contribution[2]
                contrib_dic[3]= rNo
                contrib_dic[4]= address
                results.append(contrib_dic)
        return results

    def addStrategy(self, _session_number:int, _alg_name:str, _nb_client:int, _num_round:int):
        default_account=w3.eth.accounts[2]
        federation_contract_instance.functions.add_Strategy(_session_number, _alg_name, _nb_client, _num_round).transact({'from':default_account})
        strategy = federation_contract_instance.functions.get_Strategy(_session_number).call()
        return strategy
    def addContribution(roundNumber:int, dataSize:int, address:str):
        contribution_contract_instance.functions.calculate_conribution(roundNumber, True, dataSize).transact({'from': address})
        contribution = contract_instance.functions.get_contribution(address, roundNumber).call()
        return contribution
    def addModel(self, _session_number: int, _round_number: int, _file_path: str, _file_hash: str):
        default_account=w3.eth.accounts[2]
        federation_contract_instance.functions.add_models(_session_number, _round_number, _file_path, _file_hash).transact({ 'from': default_account})
        result = federation_contract_instance.functions.get_globalModel(_session_number, _round_number).call()  
        return result
    def getTrainingSessions(self):
        addresses = contribution_contract_instance.functions.get_edge_addresses().call()
        roundNumbers = contribution_contract_instance.functions.get_rNos().call()
        results = []
        for rNo in roundNumbers:
            session_dic = {}
            dataSize = 0
            contributers = 0
            for address in addresses:
                contribution = contribution_contract_instance.functions.get_contribution(address, rNo).call()
                if contribution[1] > 0:
                    dataSize = dataSize + int(contribution[1])
                    contributers +=1
            session_dic["roundNumber"] = rNo
            session_dic["contributers"] = contributers
            session_dic["dataSize"] = dataSize
            results.append(session_dic)
        return results