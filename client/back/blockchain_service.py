from typing import Optional
from fastapi import FastAPI
from web3.auto import w3
from web3 import Web3
import json
import time
import asyncio
from threading import Thread

contributionSC = open('./config/contracts/build/Contribution.json',)
federationSC = open('./config/contracts/build/federation.json',)
contributionData = json.load(contributionSC)
federationData = json.load(federationSC)
contributionAbi = contributionData['abi']
federationAbi = federationData['abi']
addressContribution = contributionData["networks"]['5777']["address"]
addressFederation = federationData["networks"]['5777']["address"]
w3 = Web3(Web3.HTTPProvider('http://127.0.0.1:8545'))
contribution_contract_instance = w3.eth.contract(address=addressContribution, abi=contributionAbi)
federation_contract_instance = w3.eth.contract(address=addressFederation, abi=federationAbi)
class BlockchainService():
    def addWeight(self, _session_number: int, _round_number: int, _data_size: int, _file_path: str, _file_hash: str):
        default_account=w3.eth.accounts[1]
        federation_contract_instance.functions.add_weights(_session_number, _round_number, _data_size, _file_path, _file_hash).transact({ 'from': default_account})
        result = federation_contract_instance.functions.get_weight(_session_number, _round_number).call()
        return result
    def getAddress(self):
        return w3.eth.accounts[1]
    def getContributions(address):
        roundNumbers = contribution_contract_instance.functions.get_rNos().call()
        contributions = []
        for rNo in roundNumbers:
            contribution = contribution_contract_instance.functions.get_contribution(w3.eth.accounts[2], rNo).call()
            if contribution[1]>0:
                contrib_dic={}
                contrib_dic[0]= contribution[0]
                contrib_dic[1]= contribution[1]
                contrib_dic[2]= contribution[2]
                contrib_dic[3]= rNo
                contributions.append(contrib_dic)
        return contributions
def handle_event(event):
    print(event)
    from client_api import FlLunch
    flLunch = FlLunch()
    flLunch.start()

def log_loop(event_filter, poll_interval):
    print(event_filter.get_new_entries())
    while True:
        for event in event_filter.get_new_entries():
            handle_event(event)
        time.sleep(poll_interval)

def main():
    block_filter = federation_contract_instance.events.addStrategyEvent.createFilter(fromBlock='latest')
    worker = Thread(target=log_loop, args=(block_filter, 5), daemon=True)
    worker.start()
        # .. do some other stuff
main()