import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { ViewComponent } from './components/CRUD/view/view.component';
import { CreateComponent } from './components/CRUD/create/create.component';
import { ContributionsComponent } from './components/contributions/contributions.component';
import { SessionsComponent } from './components/sessions/sessions.component';
import { DataComponent } from './components/data/data.component';

const routes: Routes = [
  {path: '', redirectTo: 'data', pathMatch: 'full'},
  {path: "dashboard", component: DashboardComponent},
  {path: "view", component: ViewComponent},
  {path: "data", component: DataComponent},
  {path: "contributions", component: ContributionsComponent},
  {path: "sessions", component: SessionsComponent},
  {path: "create", component: CreateComponent},
  {path: "login", component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
