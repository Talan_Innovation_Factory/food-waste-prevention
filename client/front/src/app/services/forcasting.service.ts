import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Prediction} from '../models/prediction'
import { Observable } from 'rxjs';
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
@Injectable({
  providedIn: 'root'
})
export class ForcastingService {


  constructor(public http: HttpClient) {
  }
  url: string = "http://localhost:8001/predictFood"; 
  
  

  setPrediction(prediction: Prediction): Observable<any> {
    return this.http.post(this.url , prediction , httpOptions);
  }


}
