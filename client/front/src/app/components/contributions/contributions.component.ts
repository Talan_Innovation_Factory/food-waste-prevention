import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MainService } from 'src/app/services/main.service';

@Component({
  selector: 'app-contributions',
  templateUrl: './contributions.component.html',
  styleUrls: ['./contributions.component.scss']
})
export class ContributionsComponent implements OnInit {
  contributions: any;
  constructor(private mainService: MainService) { }

  ngOnInit() {
    this.mainService.getContributions().subscribe(res =>{
      console.log(res)
      this.contributions = res;
    })
  }
  

}
