import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})

export class DataComponent implements OnInit {
  columns: string[] = [];
  datas = [];
  filename = "";
  constructor() { }
  
  ngOnInit() {
  }
  onFileSelected(event) {

    const file:File = event.target.files[0];

    if (file) {
        this.filename = file.name;
        let reader: FileReader = new FileReader();
        reader.readAsText(file);
        reader.onload = (e) => {
           let csv: string = reader.result as string;
           console.log(csv);
           let csvToRowArray = csv.split("\n");
      this.columns = csvToRowArray[0].split(",");
      let results = []
              for (let index = 1; index < csvToRowArray.length; index++) {
              let row = csvToRowArray[index].split(",");
              results.push(row);
            }
            this.datas = results;
        }
    }
}
}
